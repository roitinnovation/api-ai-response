from setuptools import setup

setup(
    name = "response_formatter",
    url='https://bitbucket.org/roitinnovation/api-ai-response',
    author = "Leonardo Nakatani Moretti",
    author_email = "leonardo.moretti@roit.com",
    packages=['response_formatter'],
    install_requires=[],
    version = "1.0.0",
    license='MIT',
    description = ("Response Fomatter to ROIT Artificial Intelligence."),
)
