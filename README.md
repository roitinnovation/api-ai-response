# ADDING  RESPONSE FORMATTER:

response_formatter.formatter has one core function:

```
response_assembler(status,message,data,error_msg=None,error_code=None,metadata=[])
```

<strong>response_assembler</strong> receives the parameters above and assembles an API response that is correctly formatted

About the parameters:

* **status** ="success", "failed" should only be success or failed <br>
* **message(string)**  main message that will be returned, describing what  happened <br>
* **data** when the API needs to send information, pass it in this paramenter, otherwise, pass None.(In case of error, pass None) <br>
* **error_msg(string)** is the error message, must only be used when passing handled errors <br>
* **error_code(int)** code related to the specific type of error, unknown errors don’t have error codes <br>
* **metadata(list)** reserved for the future <br>

## STEP BY STEP OF HOW TO INSTALL

### STEP 1 
Go to requirements.txt and paste:
```
git+https://bitbucket.org/roitinnovation/api-ai-response.git
```
### STEP 2 
Import in the file you want to use the response_assembler

```
from response_formatter.formatter import response_assembler
```
### STEP 3

Substitute the part of the code where you manually constructed your response dict

```
    response=response_assembler("success","Document processed with success",res)
        return response, 200

 except jsonschema.exceptions.ValidationError as e:
     print("LOG[ERROR] ", e)
     response=response_assembler("failed","Input provided isn't right",None,str(e),1)
     return response, 200
```
If you are using a finally structure, try using the response formatter only on the final assembly

```
if unknown_error is not None:
    raise unknown_error
else:
    if error_object is not None:
    response = response_assembler(status,message,None,error_object['message'],error_object['code'])
    else:
        response =response_assembler(status,message,None)
        return response, http_code
```