def response_check(api_response):
    #checks if status follows pattern status is not following pattern. Should be either 'success' or 'failed'
    if api_response['status'] not in ['success','failed']:
        raise Exception("status is not following pattern. Should be either 'success' or 'failed'")
    #checks if status is success and error is not None
    if api_response['status']=='success' and api_response['error'] is not None:
        raise Exception("if status is 'success', error should be None OR if error is not None, status should be 'failed'")
    #checks keys inside of error key
    if api_response['status']=='failed' and api_response['error'] is None:
        raise Exception("if status is 'failed', error should NOT be None")

def response_assembler(status,message,data,error_msg=None,error_code=None,metadata=[]):
    api_response ={}
    api_response['status']=status.lower()
    api_response['message']=message.lower()
    api_response['data']=data
    
    if error_msg is not None or error_code is not None:
        error=error_assembler(error_msg,error_code,metadata)
    else:
        error=None 
    
    api_response['error']=error
    response_check(api_response)
    
    return api_response

def error_assembler(error_msg,error_code,metadata):
    error={}
    if error_msg is None or error_code is None:
        raise Exception('if error is not None, there should be a message and a code')
    if isinstance(error_code, int)==False:
        raise Exception("error code should be a integer number")
    if isinstance(metadata, list)==False:
        raise Exception("matadata should be of type list")
    error['message']=error_msg.lower()
    error['code']=error_code
    error['metadata']=metadata
    return error